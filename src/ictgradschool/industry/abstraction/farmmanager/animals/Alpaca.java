package ictgradschool.industry.abstraction.farmmanager.animals;

public class Alpaca extends Animal {
    private static int MAX_VALUE = 2000;

    public Alpaca() {
        value = 1600;
    }

    public void feed() {
        if (value < MAX_VALUE) {
            value = value + value / 20;
        }
    }

    public int costToFeed() {
        return 100;
    }

    public String getType() {
        return "Alpaca";
    }

    public String toString() {
        return getType() + " - $" + value;
    }
}
