package ictgradschool.industry.abstraction.pets;

/**
 * Main program.
 */
public class AnimalApp {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // TODO Populate the pets array with a Bird, a Dog and a Horse.
        animals[0] = new Bird();
        animals[1] = new Dog();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        for (int i = 0; i < list.length; i++) {
            //print eg (myName) says (sayHello)
            System.out.println(list[i].myName() + " says " + list[i].sayHello() + ".");

            //print eg (myName) is a (if false=non-mammal, if true mammal)
            System.out.print(list[i].myName() + " is a ");
            if (list[i].isMammal()) {
                System.out.println("mammal.");
            } else {
                System.out.println("non-mammal.");
            }

            //print eg "Did I forget to tell you that I have (legCount) legs.
            System.out.println("Did I forget to tell you that I have " + list[i].legCount() + " legs.");

            //do the iFamous
            if (list[i] instanceof IFamous){
                System.out.println("This is a famous name of my animal type: " + ((IFamous)list[i]).famous());
            }

            System.out.println("-----------------------------------------------");
        }
        // TODO Loop through all the pets in the given list, and print their details as shown in the lab handout.
        // TODO If the animal also implements IFamous, print out that corresponding info too.
    }

    public static void main(String[] args) {
        new AnimalApp().start();
    }
}
